#!/usr/bin/env node
'use strict';
const querystring = require('querystring');
const https = require('http');
let argv = require('yargs')
	.usage('$0 -u <username> -p <password>')
	.alias('u', 'username')
	.alias('p', 'password')
	.alias('d', 'daemon')
	.default('d', 300000)
    .describe('u', 'Your username')
    .describe('p', 'Your password in plain text or MD5/SHA256')
    .describe('d', 'Check every <d> miliseconds')
	.demandOption(['u', 'p'])
	.argv
	
setInterval(function(){
	let data = {
		username: argv.username,
		password: argv.password
	};
	let req = https.get('http://api.dynu.com/nic/update?' + querystring.stringify(data), function(res){
		res.setEncoding('utf8');
		res.on('data', function(data1){
			console.log(data1);
		});
	});
}, argv.daemon);