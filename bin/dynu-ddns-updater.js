#!/usr/bin/env node
'use strict';

let ddns = require('../index.js');
let storage = require('node-persist');
let moment = require('moment');
const fs = require('fs');

let argv = require('yargs')
	.usage('$0 --config [Path to JSON config file]')
	.config()
	.demandOption(['config'], 'Please provide \'--config [Path to JSON config file]\' arguments to work with this tool')
	.argv

storage.initSync();
setInterval(function(){
	if(storage.getItemSync('ip') != ip){
		storage.setItemSync('ip', ip);
		let config = {};
		config.ip = ip;
		config.password = argv.password;
		config.username = argv.username;
		ddns.update(config, function(response){
			if(!response.includes('nochg')){
				console.log(moment().format('DD-MM-YYYY HH:mm:ss') + '|' + response)
			}
		});
	}
}, 5000);